import React, { Component } from 'react'
import Show from './Show';
import Total from './Total';
export default class Addexpense extends Component {
    constructor(props){
        super(props);
        let expensedate =JSON.parse(localStorage.getItem('data'));
        if(expensedate)
        {
            this.state = {
                data: expensedate,
                category:'',
                expense: '',
                price: '',
                total: '',
            }
        }else{
            this.state = {
                category:'',
                expense: '',
                data:[],
                price: '',
                total: '',
               
            }
        }
        this.add=this.add.bind(this);
        this.update=this.update.bind(this);
        this.delete = this.delete.bind(this);
        this.total = this.total.bind(this);
        
    }
    add(e){
        e.preventDefault();
        let data=this.state.data;
        // console.log('data: ',data);
        let category=this.state.category;
        let expense=this.state.expense;
        let price = this.state.price;
        if (price || expense || category){
            data.push({ category:category,expense: expense, price: price, delete: false });

            console.log("updated data: ",data);
            this.setState({ data: data, category:'', expense: '', price: '' });

            localStorage.setItem('data', JSON.stringify(data));
            this.total();
            
        }
       
    }
    update(event){
        let category=this.state.category;
        
        console.log(category);
        let expense= this.state.expense;
        console.log(expense)
        let price=this.state.price;
        console.log(price)
        event.target.id === 'category' ? (this.setState({ expense : event.target.value })) : (this.setState({ price : event.target.value}))
        
        let check=this.state;
        console.log('check:',check);
    }
   
    delete(index){
        console.log(index);
        let data=this.state.data;
        data.splice(index,1);
        this.setState({data});
        localStorage.setItem('data', JSON.stringify(data));
        this.total();
    }
    total(){
        let data=this.state.data;
        if(data)
        {
            let result = 0;
            data.forEach(element => {
                console.log('total:', element);
                result += parseFloat(element.price);
            });
            console.log('result', result);
            this.setState({ total: result });
        }
     
    }
    componentDidMount(){
        this.total();
    }
  render() {
    return (
        <>
      <div>
           <form>
           <label>
          <h1 className='expe'>Expense list</h1>
          <select className='option' id='category' value={this.state.category}  onChange={this.update}>
            <option value="Travel">Travel</option>
            <option value="Food">Food</option>
            <option value="HOME">Home</option>
            </select>
        </label>
                <input type="text" className="expense" onChange={this.update} id="expense" placeholder="Expense:" value={this.state.expense}/>
                <input type="number" className="price" onChange={this.update} id="price" placeholder="Price:" value={this.state.price}/>
                <button type="submit" className="add" onClick={this.add}>Add</button>
        </form>
      </div>
      <div className='show'>
        <ul >
            {this.state.data.map((data,index)=>{
                console.log('map: ',data);
                       return <Show data={data} index={index} key={index} delete={this.delete}/>
            })}
        </ul>
        <Total total={this.state.total}/>
        </div>
          
      </>
    )
  }
}
