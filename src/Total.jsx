import React from 'react'

function Total(props) {
  return (
    <div className="total">
    Total: {props.total}
    </div>
  )
}

export default Total